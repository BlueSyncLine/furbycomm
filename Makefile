all: clean test-boost-demod boom-scan

clean:
	rm -f *.o test-boost-demod boom-scan

test-boost-demod: test-boost-demod.o modem.o comair.o
	gcc -lm test-boost-demod.o modem.o comair.o -o test-boost-demod

boom-scan: boom-scan.o furby.o furbydata.o modem.o comair.o
	gcc -lm `sdl2-config --libs` boom-scan.o furby.o furbydata.o modem.o comair.o -o boom-scan

test-boost-demod.o:
	gcc -Wall -Wextra -g -c test-boost-demod.c -o test-boost-demod.o

boom-scan.o:
	gcc -Wall -Wextra -g -c boom-scan.c -o boom-scan.o

modem.o:
	gcc -Wall -Wextra -g -c modem.c -o modem.o

comair.o:
	gcc -Wall -Wextra -g -c comair.c -o comair.o

furby.o:
	gcc -Wall -Wextra -g -c furby.c -o furby.o

furbydata.o:
	gcc -Wall -Wextra -g -c furbydata.c -o furbydata.o
