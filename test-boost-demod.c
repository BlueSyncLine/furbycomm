#include <stdlib.h>
#include <stdio.h>

#include "modem.h"

#define SAMPLE_RATE 44100
#define BUF_SIZE 256

void callback_ch1(int data) {
    printf("Channel 1: %08x\n", data);
}

void callback_ch2(int data) {
    printf("Channel 2: %08x\n", data);
}

int main(int argc, char *argv[]) {
    float buf[BUF_SIZE];
    comAirParams_t channel1_params, channel2_params;
    comAirDecoder_t channel1, channel2;

    // Configure the two ComAir channels.
    channel1_params.centerFrequency = 17000;
    channel1_params.frequencyDelta = 100;
    channel1_params.boostMode = 1;
    channel2_params.centerFrequency = 18000;
    channel2_params.frequencyDelta = 100;
    channel2_params.boostMode = 1;

    if (!comAirDecoder_init(&channel1, channel1_params, SAMPLE_RATE, callback_ch1)) {
        perror("Failed to initialize the channel 1 decoder");
        return EXIT_FAILURE;
    }

    if (!comAirDecoder_init(&channel2, channel2_params, SAMPLE_RATE, callback_ch2)) {
        comAirDecoder_destroy(&channel1);
        perror("Failed to initialize the channel 2 decoder");
        return EXIT_FAILURE;
    }

    // While input samples are available, feed them to the decoders.
    while (fread(buf, sizeof(float), BUF_SIZE, stdin) == BUF_SIZE) {
        comAirDecoder_input(&channel1, buf, BUF_SIZE);
        comAirDecoder_input(&channel2, buf, BUF_SIZE);
    }

    comAirDecoder_destroy(&channel1);
    comAirDecoder_destroy(&channel2);
    return EXIT_SUCCESS;
}
