#ifndef FURBY_H
#define FURBY_H

// Command code for requesting a Boom Furby's status data.
#define FURBY_COMMAND_STATUS 427

// SwizzleBoostFields from the application.
#define SWIZZLE_BOOST_FIELDS(x) (((x) & 0x03f000fc0ull) | ((x) & 0xfc003full) << 12 | ((x) & 0xfc003f000ull) >> 12)

typedef struct {
    int pattern;
    int name;
    int personality;
    int sickness;

    // sic!
    int happyness;

    int fullness;
} furbyStatus_t;

void furbyStatus_parse(furbyStatus_t *furbyStatus, int ch1, int ch2);

#endif
