#include <string.h>
#include "comair.h"

// Count of delta-fs for every dibit.
const int COMAIR_SYMBOL_DF_MAPPING[5] = {
    -2, -1, 2, 1, 0
};

// Checksums for all 64 possible 6-bit groups.
const char COMAIR_GROUP_CHECKSUMS[64][4] = {
    {0, 0, 0, 0},
    {0, 1, 1, 0},
    {0, 2, 1, 0},
    {0, 3, 0, 0},
    {1, 0, 2, 3},
    {1, 1, 3, 3},
    {1, 2, 3, 3},
    {1, 3, 2, 3},
    {0, 1, 2, 0},
    {0, 2, 0, 1},
    {0, 3, 3, 0},
    {1, 0, 2, 1},
    {1, 1, 0, 3},
    {1, 2, 2, 2},
    {1, 3, 1, 3},
    {2, 0, 2, 1},
    {0, 2, 2, 0},
    {0, 3, 3, 0},
    {1, 0, 0, 0},
    {1, 1, 1, 0},
    {1, 2, 0, 3},
    {1, 3, 1, 3},
    {2, 0, 0, 0},
    {2, 1, 1, 0},
    {0, 3, 0, 0},
    {1, 0, 1, 1},
    {1, 1, 2, 0},
    {1, 2, 0, 1},
    {1, 3, 2, 3},
    {2, 0, 1, 1},
    {2, 1, 2, 0},
    {2, 2, 0, 1},
    {1, 0, 3, 3},
    {1, 1, 2, 3},
    {1, 2, 2, 3},
    {1, 3, 3, 3},
    {2, 0, 3, 3},
    {2, 1, 2, 3},
    {2, 2, 2, 3},
    {2, 3, 3, 3},
    {1, 1, 1, 3},
    {1, 2, 3, 2},
    {1, 3, 0, 3},
    {2, 0, 3, 1},
    {2, 1, 1, 3},
    {2, 2, 3, 2},
    {2, 3, 0, 3},
    {3, 0, 1, 2},
    {1, 2, 1, 3},
    {1, 3, 0, 3},
    {2, 0, 1, 0},
    {2, 1, 0, 0},
    {2, 2, 1, 3},
    {2, 3, 0, 3},
    {3, 0, 3, 3},
    {3, 1, 2, 3},
    {1, 3, 3, 3},
    {2, 0, 0, 1},
    {2, 1, 3, 0},
    {2, 2, 1, 1},
    {2, 3, 3, 3},
    {3, 0, 2, 2},
    {3, 1, 1, 3},
    {3, 2, 3, 2}
};

// A 4-symbol suffix for every packet.
const char COMAIR_PACKET_SUFFIX[4] = {
    1, 0, 3, 2
};


// Packs a 6-bit group with a corresponding checksum.
void comAirEncodeGroup(char group[7], char value) {
    // Unpack the value.
    group[0] = (value >> 4) & 3;
    group[1] = (value >> 2) & 3;
    group[2] = value & 3;

    // Copy the checksum.
    memcpy(group + 3, COMAIR_GROUP_CHECKSUMS[(int)value], 4);
}

// Tries decoding a checksummed 6-bit group, returns either the decoded value
//  or -1.
char comAirDecodeGroup(const char group[7]) {
    char value;

    // Unpack the value.
    value = group[0] << 4 | group[1] << 2 | group[2];

    // Test the checksum.
    if (memcmp(group + 3, COMAIR_GROUP_CHECKSUMS[(int)value], 4) != 0)
        return -1;

    return value;
}

// Packs a complete regular 6-bit packet.
void comAirEncodePacket(char packet[COMAIR_PACKET_LENGTH], char value) {
    // Set the packet prefix.
    packet[0] = 3;

    // Encode the group.
    comAirEncodeGroup(packet + 1, value);

    // Copy the suffix.
    memcpy(packet + 8, COMAIR_PACKET_SUFFIX, 4);
}

// Packs a Boost packet with a 18-bit value.
void comAirEncodeBoostPacket(char packet[COMAIR_BOOST_PACKET_LENGTH], int value) {
    // Set the packet prefix.
    packet[0] = 3;

    // Encode the groups, starting with the most significant bits of the value.
    comAirEncodeGroup(packet + 1, (value >> 12) & 0x3f);
    comAirEncodeGroup(packet + 8, (value >> 6) & 0x3f);
    comAirEncodeGroup(packet + 15, value & 0x3f);

    // Copy the suffix.
    memcpy(packet + 22, COMAIR_PACKET_SUFFIX, 4);
}

// Tries to decode a regular packet, returns either the decoded value or -1 on failure.
char comAirDecodePacket(const char packet[COMAIR_PACKET_LENGTH]) {
    // Check the prefix first.
    if (packet[0] != 3)
        return -1;

    // Check the suffix.
    if (memcmp(packet + 8, COMAIR_PACKET_SUFFIX, 4) != 0)
        return -1;

    return comAirDecodeGroup(packet + 1);
}

// Tries to decode a Boost packet, returns either the decoded value or -1 on failure.
int comAirDecodeBoostPacket(const char packet[COMAIR_BOOST_PACKET_LENGTH]) {
    int x, y, z;

    // Check the prefix first.
    if (packet[0] != 3)
        return -1;

    // Check the suffix.
    if (memcmp(packet + 22, COMAIR_PACKET_SUFFIX, 4) != 0)
        return -1;

    // Try decoding the first group.
    if ((x = comAirDecodeGroup(packet + 1)) == -1)
        return -1;

    // Try decoding the second group.
    if ((y = comAirDecodeGroup(packet + 8)) == -1)
        return -1;

    // Try decoding the third group.
    if ((z = comAirDecodeGroup(packet + 15)) == -1)
        return -1;

    // Pack into a 18-bit integer.
    return x << 12 | y << 6 | z;
}
