#ifndef MODEM_H
#define MODEM_H

#include <math.h>
#include <complex.h>
#include "comair.h"

// Type declaration for the callback function.
typedef void (*comAirDecoder_callback)(int);

typedef struct {
    int centerFrequency;
    int frequencyDelta;
    int boostMode;
} comAirParams_t;

typedef struct {
    comAirParams_t params;
    int sampleRate;
    int symbolDuration;

    float *buffer;
    int bufferLength;
} comAirEncoder_t;

typedef struct {
    comAirParams_t params;
    int sampleRate;
    comAirDecoder_callback callback;

    int symbolDuration;
    int symbolDelayLength;

    float complex *filters[4];
    char *symbolDelayLine;
    int t;
    int previousValue;
} comAirDecoder_t;


int comAirEncoder_init(comAirEncoder_t *encoder, comAirParams_t params, int sampleRate);
void comAirEncoder_destroy(comAirEncoder_t *encoder);
void comAirEncoder_encode(comAirEncoder_t *encoder, int value);

int comAirDecoder_init(comAirDecoder_t *decoder, comAirParams_t params, int sampleRate, comAirDecoder_callback callback);
void comAirDecoder_destroy(comAirDecoder_t *decoder);
void comAirDecoder_input(comAirDecoder_t *decoder, float *buffer, int bufferLength);
#endif
