#ifndef COMAIR_H
#define COMAIR_H

// A regular packet is 12 symbols long.
#define COMAIR_PACKET_LENGTH 12

// A Boost packet takes up 26 symbols.
#define COMAIR_BOOST_PACKET_LENGTH 26

// The value 4 is used as a inter-symbol sync.
#define COMAIR_SYNC_SYMBOL 4

// The symbol clock frequency is 50 Hz.
#define COMAIR_SYMBOL_CLOCK 50

const int COMAIR_SYMBOL_DF_MAPPING[5];
const char COMAIR_GROUP_CHECKSUMS[64][4];
const char COMAIR_PACKET_SUFFIX[4];

void comAirEncodeGroup(char group[7], char value);
char comAirDecodeGroup(const char group[7]);

void comAirEncodePacket(char packet[COMAIR_PACKET_LENGTH], char value);
char comAirDecodePacket(const char packet[COMAIR_PACKET_LENGTH]);

void comAirEncodeBoostPacket(char packet[COMAIR_BOOST_PACKET_LENGTH], int value);
int comAirDecodeBoostPacket(const char packet[COMAIR_BOOST_PACKET_LENGTH]);
#endif
