#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <SDL2/SDL.h>

#include "modem.h"
#include "furby.h"
#include "furbydata.h"

#define SAMPLE_RATE 44100
#define BUFFER_SIZE 1024

int ch1_data, ch2_data;
comAirDecoder_t ch1, ch2;
comAirEncoder_t encoder;
SDL_AudioDeviceID capture, playback;
SDL_mutex *dataMutex;
SDL_sem *responseSem;

// Audio callback, feed data into the decoders.
void audioCallback(void *userdata, Uint8 *stream, int len) {
    //fprintf(stderr, "audio callback! %f\n", ((float*)stream)[0]);
    comAirDecoder_input(&ch1, (float*)stream, len / sizeof(float));
    comAirDecoder_input(&ch2, (float*)stream, len / sizeof(float));
}

// Initialize audio recording and such.
int initAudio() {
    SDL_AudioSpec want, have;
    SDL_zero(want);

    // Get a capture device first.
    want.freq = SAMPLE_RATE;
    want.format = AUDIO_F32;
    want.channels = 1;
    want.samples = BUFFER_SIZE;
    want.callback = audioCallback;

    if (!(capture = SDL_OpenAudioDevice(NULL, 1, &want, &have, 0))) {
        fprintf(stderr, "Couldn't open the capture device: %s\n", SDL_GetError());
        return 0;
    }

    // The playback device will be QueueAudio-driven.
    want.callback = NULL;
    if (!(playback = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0))) {
        fprintf(stderr, "Couldn't open the playback device: %s\n", SDL_GetError());
        return 0;
    }

    // Unpause the capture/playback devices.
    SDL_PauseAudioDevice(capture, 0);
    SDL_PauseAudioDevice(playback, 0);

    return 1;
}

// Free the memory and release resources.
void finish() {
    fprintf(stderr, "finish called!\n");

    if (capture)
        SDL_CloseAudioDevice(capture);

    if (playback)
        SDL_CloseAudioDevice(playback);

    if (dataMutex) {
        SDL_UnlockMutex(dataMutex);
        SDL_DestroyMutex(dataMutex);
    }

    if (responseSem) {
        SDL_SemPost(responseSem);
        SDL_DestroySemaphore(responseSem);
    }

    SDL_Quit();

    comAirDecoder_destroy(&ch1);
    comAirDecoder_destroy(&ch2);
    comAirEncoder_destroy(&encoder);
}

// Signal handler that'll call finish even if we're ^C'd
void signalHandler(int signum) {
    finish();
    exit(EXIT_FAILURE);
}

// Send a 2012/Boom command number.
void sendCommand(int value) {
    comAirEncoder_encode(&encoder, value >> 5);
    SDL_QueueAudio(playback, encoder.buffer, encoder.bufferLength * sizeof(float));
    SDL_Delay(1000);

    comAirEncoder_encode(&encoder, (value & 0x1f) | 0x20);
    SDL_QueueAudio(playback, encoder.buffer, encoder.bufferLength * sizeof(float));
    SDL_Delay(1000);
}

void responseReceived() {
    SDL_SemPost(responseSem);
}

void callback_ch1(int value) {
    fprintf(stderr, "17 kHz: %05x\n", value);
    SDL_LockMutex(dataMutex);
    ch1_data = value;

    // If there's also data from channel 2...
    if (ch2_data != -1)
        responseReceived();

    SDL_UnlockMutex(dataMutex);
}

void callback_ch2(int value) {
    fprintf(stderr, "18 kHz: %05x\n", value);
    SDL_LockMutex(dataMutex);
    ch2_data = value;

    // If there's also data from channel 1...
    if (ch1_data != -1)
        responseReceived();

    SDL_UnlockMutex(dataMutex);
}

int main(int argc, char *argv[]) {
    furbyStatus_t furbyStatus;
    comAirParams_t ch1_params, ch2_params, encoderParams;

    // Initialize the variables
    capture = 0;
    playback = 0;

    // Initialize the ComAir encoder and decoders.
    ch1_params.centerFrequency = 17000;
    ch1_params.frequencyDelta = 100;
    ch1_params.boostMode = 1;
    ch2_params.centerFrequency = 18000;
    ch2_params.frequencyDelta = 100;
    ch2_params.boostMode = 1;
    encoderParams.centerFrequency = 17500;
    encoderParams.frequencyDelta = 562;
    encoderParams.boostMode = 0;

    if (!comAirDecoder_init(&ch1, ch1_params, SAMPLE_RATE, callback_ch1)) {
        perror("Couldn't allocate memory for channel 1 decoder");
        return EXIT_FAILURE;
    }

    if (!comAirDecoder_init(&ch2, ch2_params, SAMPLE_RATE, callback_ch2)) {
        comAirDecoder_destroy(&ch1);
        perror("Couldn't allocate memory for channel 2 decoder");
        return EXIT_FAILURE;
    }

    if (!comAirEncoder_init(&encoder, encoderParams, SAMPLE_RATE)) {
        comAirDecoder_destroy(&ch1);
        comAirDecoder_destroy(&ch2);
        perror("Couldn't allocate memory for the encoder");
        return EXIT_FAILURE;
    }

    // Initialize SDL's audio subsystem.
    if (SDL_Init(SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        comAirDecoder_destroy(&ch1);
        comAirDecoder_destroy(&ch2);
        comAirEncoder_destroy(&encoder);
        return EXIT_FAILURE;
    }

    // Free up the resources even if we're terminated.
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    ch1_data = -1;
    ch2_data = -1;
    dataMutex = SDL_CreateMutex();
    responseSem = SDL_CreateSemaphore(0);

    // Try initializing audio capture and playback.
    if (!initAudio()) {
        finish();
        return EXIT_FAILURE;
    }

    while (1) {
        // Send the status request command until a response arrives.
        // Reset the received data.
        SDL_LockMutex(dataMutex);
        ch1_data = -1;
        ch2_data = -1;
        SDL_UnlockMutex(dataMutex);

        fprintf(stderr, "sending a status request...\n");
        sendCommand(FURBY_COMMAND_STATUS);

        // Check if we got a response.
        if (SDL_SemWaitTimeout(responseSem, 5000) == 0)
            break;
    }

    // Pause the capture device - we no longer need the data callbacks.
    SDL_PauseAudioDevice(capture, 1);

    // Parse the status values.
    furbyStatus_parse(&furbyStatus, ch1_data, ch2_data);

    // Print them.
    printf("name: %s\n", FURBY_NAMES[furbyStatus.name]);
    printf("pattern: %s\n", FURBY_PATTERN_NAMES[furbyStatus.pattern]);
    printf("personality: %s\n", FURBY_PERSONALITY_NAMES[furbyStatus.personality]);
    printf("sick: %d\n", furbyStatus.sickness);
    printf("happiness: %d%%\n", furbyStatus.happyness);
    printf("fullness: %d%%\n", furbyStatus.fullness);

    finish();
    return EXIT_SUCCESS;
}
