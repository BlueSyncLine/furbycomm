#include <stdlib.h>
#include <stdio.h>

#include <math.h>
#include <complex.h>

#include "modem.h"
#include "comair.h"

// Initializes the encoder data structure and allocates a sample buffer. Returns 1 on success and 0 on failure.
int comAirEncoder_init(comAirEncoder_t *encoder, comAirParams_t params, int sampleRate) {
    encoder->params = params;
    encoder->sampleRate = sampleRate;
    encoder->symbolDuration = sampleRate / COMAIR_SYMBOL_CLOCK;
    encoder->bufferLength = encoder->symbolDuration * ((params.boostMode ? COMAIR_BOOST_PACKET_LENGTH : COMAIR_PACKET_LENGTH) * 2 + 1);

    if (!(encoder->buffer = calloc(encoder->bufferLength, sizeof(float))))
        return 0;

    return 1;
}

// Frees up all the memory allocated.
void comAirEncoder_destroy(comAirEncoder_t *encoder) {
    free(encoder->buffer);
}

// Encodes a value.
void comAirEncoder_encode(comAirEncoder_t *encoder, int value) {
    int i, j, t, symbolCount, frequency;
    float amplitude;

    char symbol;

    // The array is of a length suitable to accomodate both the Boost and regular packets.
    char packet[COMAIR_BOOST_PACKET_LENGTH];

    // Set symbolCount and encode the packet.
    if (encoder->params.boostMode) {
        symbolCount = COMAIR_BOOST_PACKET_LENGTH * 2 + 1;
        comAirEncodeBoostPacket(packet, value);
    } else {
        symbolCount = COMAIR_PACKET_LENGTH * 2 + 1;
        comAirEncodePacket(packet, value);
    }

    // Generate the samples.
    t = 0;
    for (i = 0; i < symbolCount; i++) {
        // If i is even, use the sync symbol, otherwise pick one from the packet.
        if (i % 2 == 0)
            symbol = COMAIR_SYNC_SYMBOL;
        else
            symbol = packet[i>>1];

        // Choose the frequency.
        frequency = encoder->params.centerFrequency + encoder->params.frequencyDelta * COMAIR_SYMBOL_DF_MAPPING[(int)symbol];

        // Generate the symbol.
        for (j = 0; j < encoder->symbolDuration; j++) {
            // Use the sine function as a pulse-shaping window.
            amplitude = sinf((float)j / encoder->symbolDuration * M_PI);

            // Store the sample.
            encoder->buffer[t] = sinf((float)frequency * t * M_PI * 2.0f / encoder->sampleRate) * amplitude;
            t++;
        }
    }
}

// Allocates memory for the decoder and returns 1 on success or 0 on failure.
int comAirDecoder_init(comAirDecoder_t *decoder, comAirParams_t params, int sampleRate, comAirDecoder_callback callback) {
    int i;

    decoder->params = params;
    decoder->sampleRate = sampleRate;
    decoder->callback = callback;
    decoder->symbolDuration = sampleRate / COMAIR_SYMBOL_CLOCK;
    decoder->symbolDelayLength = decoder->symbolDuration * ((params.boostMode ? COMAIR_BOOST_PACKET_LENGTH : COMAIR_PACKET_LENGTH) * 2 + 1);
    decoder->t = 0;
    decoder->previousValue = -1;

    fprintf(stderr, "comAirDecoder: sampleRate %d, symbolDuration %d, symbolDelayLength %d\n", decoder->sampleRate, decoder->symbolDuration, decoder->symbolDelayLength);

    // Initialize the all pointers to NULLs so they can be freed even if allocation fails.
    for (i = 0; i < 4; i++)
        decoder->filters[i] = NULL;
    decoder->symbolDelayLine = NULL;

    // Allocate the sample buffers for the moving-average filters.
    for (i = 0; i < 4; i++) {
        if (!(decoder->filters[i] = calloc(decoder->symbolDuration, sizeof(float complex)))) {
            comAirDecoder_destroy(decoder);
            return 0;
        }
    }

    // Try allocating the symbol delay line.
    if (!(decoder->symbolDelayLine = calloc(decoder->symbolDelayLength, sizeof(char)))) {
        comAirDecoder_destroy(decoder);
        return 0;
    }

    // Otherwise, return 1.
    return 1;
}

// Frees all memory allocated by the decoder.
void comAirDecoder_destroy(comAirDecoder_t *decoder) {
    int i;

    for (i = 0; i < 4; i++)
        free(decoder->filters[i]);

    free(decoder->symbolDelayLine);
}


// Computes the average value of a float complex array.
float complex average(float complex *x, int length) {
    int i;
    float complex acc = 0.0f;

    for (i = 0; i < length; i++)
        acc += x[i];

    return acc / length;
}

// Finds the index of the largest element in a float array.
int max_index(float *x, int length) {
    int i, max_i;
    float max_value;

    max_i = 0;
    max_value = -INFINITY;

    for (i = 0; i < length; i++) {
        // New maximum found.
        if (x[i] > max_value) {
            max_value = x[i];
            max_i = i;
        }
    }

    return max_i;
}

// Handles the incoming samples.
void comAirDecoder_input(comAirDecoder_t *decoder, float *buffer, int bufferLength) {
    int i, j, frequency, decodedValue;
    float samp, amplitudes[4];
    char candidatePacket[COMAIR_BOOST_PACKET_LENGTH];

    for (i = 0; i < bufferLength; i++) {
        samp = buffer[i];

        // Filter the incoming sample.
        for (j = 0; j < 4; j++) {
            frequency = decoder->params.centerFrequency + decoder->params.frequencyDelta * COMAIR_SYMBOL_DF_MAPPING[j];
            decoder->filters[j][decoder->t % decoder->symbolDuration] = cexpf(-I * frequency * decoder->t * M_PI * 2.0f / decoder->sampleRate) * samp;

            // Find the current amplitude of the filtered frequency.
            amplitudes[j] = cabsf(average(decoder->filters[j], decoder->symbolDuration));
        }

        // Pick the symbol with the largest amplitude.
        decoder->symbolDelayLine[decoder->t % decoder->symbolDelayLength] = max_index(amplitudes, 4);

        // Make sure the delay lines have been populated first.
        if (decoder->t >= decoder->symbolDelayLength) {
            for (j = 0; j < (decoder->params.boostMode ? COMAIR_BOOST_PACKET_LENGTH : COMAIR_PACKET_LENGTH); j++)
                candidatePacket[j] = decoder->symbolDelayLine[(decoder->t + j * decoder->symbolDuration * 2 + 1) % decoder->symbolDelayLength];

            // Try decoding the symbols.
            if (decoder->params.boostMode)
                decodedValue = comAirDecodeBoostPacket(candidatePacket);
            else
                decodedValue = comAirDecodePacket(candidatePacket);

            // If the decoded value is new, call the callback function.
            if (decodedValue != -1 && decodedValue != decoder->previousValue) {
                decoder->previousValue = decodedValue;
                decoder->callback(decodedValue);
            }
        }

        // Increment t.
        decoder->t++;
    }
}
