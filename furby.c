#include <stdio.h>
#include <stdint.h>
#include "furby.h"

// Parse a furbyStatus structure from two Boost packets.
void furbyStatus_parse(furbyStatus_t *furbyStatus, int ch1, int ch2) {
    uint64_t data;
    data = SWIZZLE_BOOST_FIELDS((uint64_t)ch2 << 18 | (uint64_t)ch1);

    furbyStatus->pattern = (data >> 11) & 0x3f;
    furbyStatus->name = (data >> 3) & 0xff;
    furbyStatus->personality = (data >> 32) & 7;
    furbyStatus->sickness = (data >> 2) & 1;
    furbyStatus->happyness = ((data >> 28) & 15) * 10;
    furbyStatus->fullness = ((data >> 24) & 15) * 10;
}
